import React from 'react'
import moment from 'moment'
import './Chat.css'

import ChatBox from './../../components/chatBox/ChatBox'
import SiteHeader from './../../components/SiteHeader/SiteHeader'
import MessageInput from './../../components/MessageInput/MessageInput'
import Footer from './../../components/Footer/Footer'
import Spinner from './../../components/Spinner/Spinner'
import MessageModel from '../../models/MessageModel'

class Chat extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      showSpinner: true,
      chatItems: [],
      currentlyEditedMessage: null,
    }
  }

  componentDidMount() {
    fetch('https://edikdolynskyi.github.io/react_sources/messages.json')
      .then((res) => res.json())
      .then(
        (result) => {
          const chatItems = result.map((item) => new MessageModel(item))

          this.setState({
            showSpinner: false,
            chatItems,
          })
        },
        (error) => {
          console.log('smth went wrong')
        }
      )
  }

  createMessage(text) {
    const message = new MessageModel({
      id: new Date().getTime(),
      avatar: 'https://my-fake-url',
      user: 'Takeshi Kovach',
      userId: 'my-fake-id',
      text,
      createdAt: new Date(),
      editedAt: null,
      isMine: true,
      likeCount: 0,
      isMylike: false,
    })

    const chatItems = [...this.state.chatItems]
    chatItems.push(message)

    this.setState({
      ...this.state,
      chatItems,
    })
  }

  toggleLike(id) {
    const chatItems = this.state.chatItems.map((item) => {
      if (item.id === id) {
        item.isMyLike = !item.isMyLike
        item.isMyLike ? item.likeCount++ : item.likeCount--
      }
      return item
    })

    this.setState({
      ...this.state,
      chatItems,
    })
  }

  deleteMessage(id) {
    this.setState({
      ...this.state,
      chatItems: this.state.chatItems.filter((item) => item.id !== id),
    })
  }

  editMessage(id, text) {
    this.setState({
      ...this.state,
      currentlyEditedMessage: { id, text },
    })
  }

  updateMessage(text) {
    const chatItems = this.state.chatItems.map((item) => {
      if (item.id === this.state.currentlyEditedMessage.id) {
        item.text = text
        item.editedAt = moment()
      }
      return item
    })

    this.setState({
      ...this.state,
      chatItems,
      currentlyEditedMessage: null,
    })
  }

  render() {
    return (
      <div className="content-container">
        {this.state.showSpinner ? <Spinner /> : ''}
        <SiteHeader></SiteHeader>

        <ChatBox
          chatItems={this.state.chatItems}
          likeHandler={this.toggleLike.bind(this)}
          deleteHandler={this.deleteMessage.bind(this)}
          editHandler={this.editMessage.bind(this)}
        />
        <MessageInput
          currentlyEditedMessage={this.state.currentlyEditedMessage}
          messageHandler={this.createMessage.bind(this)}
          updateHandler={this.updateMessage.bind(this)}
        ></MessageInput>

        <Footer></Footer>
      </div>
    )
  }
}

export default Chat
