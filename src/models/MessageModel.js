import moment from 'moment';

export default class MessageModel {
    constructor(data) {
        this.id = data.id;
        this.avatar = data.avatar;
        this.user = data.user;
        this.userId = data.userId;
        this.text = data.text;
        this.createdAt = moment(data.createdAt);
        this.editedAt = data.editedAt ? moment(data.editedAt) : null;
        this.isMine = !!data.isMine;
        this.likeCount = 0;
        this.isMyLike = false;
    }
}