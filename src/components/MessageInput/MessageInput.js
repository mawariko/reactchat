import React, { useState, useEffect } from 'react'
import './MessageInput.css'

export default function MessageInput({
  currentlyEditedMessage,
  messageHandler,
  updateHandler,
}) {
  const [text, setText] = useState('')

  const submitHandler = () => {
    if (text) {
      if (currentlyEditedMessage) {
        updateHandler(text)
      } else {
        messageHandler(text)
      }
      setText('')
    }
  }

  useEffect(() => {
    if (currentlyEditedMessage) {
      setText(currentlyEditedMessage.text)
    }
  }, [currentlyEditedMessage])

  return (
    <div className="message-input">
      <textarea
        id="message-input"
        name="message-input"
        rows="3"
        cols="110"
        placeholder="Enter your message"
        onChange={(e) => setText(e.target.value)}
        value={text}
      ></textarea>

      <button className="send-button" onClick={submitHandler}>
        {currentlyEditedMessage ? 'EDIT' : 'SEND'}
      </button>
    </div>
  )
}
