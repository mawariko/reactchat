import React from 'react'

// import {Divider} from 'semantic-ui-react';
import { Icon } from 'semantic-ui-react'

import './Message.css'

export default function Message({
  id,
  text,
  user,
  avatar,
  editedAt,
  createdAt,
  isMine,
  likeCount,
  isMyLike,
  likeHandler,
  deleteHandler,
  editHandler,
}) {
  return (
    <div className={`message-box${isMine ? ' my-message' : ''}`}>
      <div className="message-details">
        <p>{user}</p>
        <p>{createdAt.format('h:mm A')}</p>
        {editedAt && (
          <span className="edited">&#9998;{editedAt.format('h:mm A')}</span>
        )}
      </div>
      <div className="message-content">
        {!isMine && <img src={avatar} alt="user" />}
        <p>{text}</p>
        {!isMine && (
          <>
            {likeCount > 0 && <span>{likeCount}</span>}
            <span
              onClick={() => likeHandler(id)}
              className={`icon message-heart ${isMyLike ? 'my-like' : ''}`}
            >
              &#9825;
            </span>
          </>
        )}
        {isMine && (
          <div className="my-icons">
            <span onClick={() => deleteHandler(id)} className="icon message-trash">
              &#128465;
            </span>
            <span
              onClick={() => editHandler(id, text)}
              className="icon message-edit"
            >
              &#9998;
            </span>
          </div>
        )}
      </div>
    </div>
  )
}
