import React, { useEffect, useRef } from 'react'
import Message from '../Message/Message'
import MessageDivider from '../MessageDivider/MessageDivider'

export default function MessageList({
  chatItems,
  likeHandler,
  deleteHandler,
  editHandler,
}) {
  const messagesEndRef = useRef(null)

  const scrollToBottom = () => {
    messagesEndRef.current.scrollIntoView({ behavior: 'smooth' })
  }

  const items = []
  let lastDate = null
  chatItems.forEach((chatItem) => {
    const date = chatItem.createdAt.format('DD MMMM')
    if (date !== lastDate) {
      lastDate = date
      items.push(<MessageDivider date={chatItem.createdAt} key={lastDate} />)
    }

    items.push(
      <Message
        {...chatItem}
        key={chatItem.id}
        likeHandler={likeHandler}
        deleteHandler={deleteHandler}
        editHandler={editHandler}
      ></Message>
    )
  })

  useEffect(scrollToBottom, [items])
  return (
    <div>
      {items} <div ref={messagesEndRef} />
    </div>
  )
}
