import React from 'react'
import ChatHeader from './../ChatHeader/ChatHeader'
import MessageList from './../MessageList/MessageList'

import './ChatBox.css'

export default function ChatBox({ chatItems, likeHandler, deleteHandler, editHandler }) {
  const userNames = new Set(chatItems.map((user) => user.user))
  const lastMessageDate = chatItems.length ? chatItems[chatItems.length - 1].createdAt : null

  return (
    <div className="chat-box">
      <ChatHeader
        uniqueUsers={userNames.size}
        messageNumber={chatItems.length}
        lastMessageDate={lastMessageDate}
      />
      <MessageList chatItems={chatItems} likeHandler={likeHandler} deleteHandler={deleteHandler} editHandler={editHandler}/>
    </div>
  )
}
